######
# Flask
from flask import Flask, render_template, request, redirect, send_from_directory, g
from flask import session, url_for, escape
from werkzeug import secure_filename
from jinja2 import Environment, PackageLoader

# base de datos
from database import init_db, db_session
from database import text, engine

# tablas
from models import Usuario, Queja, Comentario, Registro, Ingreso, Multimedia

# Misc
from flask import json, jsonify, abort, flash
from flask.ext.login import login_user, logout_user, current_user, login_required
from flask.ext.login import LoginManager
#from flask.ext.openid import OpenID
import datetime
import hashlib

app = Flask(__name__)
app.secret_key = ':\x16j\x93\xe9\xc5e\xcc\xb8\x14\xe9fE\x9b;\xf7\xf7\xfa\x10\x8b{\xbc\x17\xcf'

login_manager = LoginManager()
login_manager.init_app(app)

@app.before_request
def before_request():
    g.user = current_user

@login_manager.user_loader
def load_user(id):
    return Usuario.query.get(int(id_usuario))

########### Rutas de pruebas (no tomar en cuenta) ################
# pagina principal
@app.route('/')
@app.route('/index')
def pag_principal():
    # comprueba si existe usuario logueado
    if 'username' in session:
        return render_template("index.html", usuario_logueado=session['username'])
    else:
        return render_template("index.html", usuario_logueado='Anonimo')

@app.route('/ls_n', methods=['GET', 'POST'])
def ls_n():
    # consulta = text("SELECT q.id_queja, q.id_usuario, q.titulo, q.likes, q.dislikes, q.visible, u.nombre",\
    #                 "FROM :q q, :u u",\
    #                 "WHERE q.id_usuario = u.id_usuario ")
    # res = engine.execute(consulta, {"q":"queja", "u":"usuario"})

    # consulta = text("SELECT q.id_queja, q.id_usuario, q.titulo, q.likes, q.dislikes, q.visible, u.nombre",\
    #                 "FROM queja q, usuario u",\
    #                 "WHERE q.id_usuario = u.id_usuario")
    consulta = text("""SELECT q.id_queja, q.id_usuario, q.titulo, q.likes, q.dislikes, q.visible, u.nombre
    FROM queja q, usuario u 
    WHERE q.id_usuario = u.id_usuario""")

    print str(engine.execute(consulta).fetchall())

    #res = engine.execute(consulta).keys()
    res = str(engine.execute(consulta).fetchall())

    return json.dumps(res)
    #return str(engine.execute(consulta).keys()) + res
##############################################################


####### Listado de tablas #######
@app.route('/ls_usuarios', methods=['GET', 'POST'])
def ls_usuarios():
    if request.method == 'GET':
        return jsonify(json_list=[i.serialize for i in Usuario.query.all()])

@app.route('/ls_quejas')
def ls_quejas():
    #print jsonify(json_list=[i.serialize for i in Queja.query.all()])
    #return jsonify(json_list=[i.serialize for i in Queja.query.all()])
    li = []
    for i in Queja.query.all():
        li.append(i.serialize)
    j = json.dumps(li, sort_keys=True, indent=4)
    
    return j

@app.route('/ls_comentarios')
def ls_comentarios():
    print jsonify(json_list=[i.serialize for i in Comentario.query.all()])
    return jsonify(json_list=[i.serialize for i in Comentario.query.all()])

@app.route('/ls_registros')
def lista_registros():
    print jsonify(json_list=[i.serialize for i in Registro.query.all()])
    return jsonify(json_list=[i.serialize for i in Registro.query.all()])
    
@app.route('/ls_ingresos')
def lista_ingresos():
    print jsonify(json_list=[i.serialize for i in Ingreso.query.all()])
    return jsonify(json_list=[i.serialize for i in Ingreso.query.all()])

@app.route('/ls_multimedias')
def lista_multimedias():
    print jsonify(json_list=[i.serialize for i in Multimedia.query.all()])
    return jsonify(json_list=[i.serialize for i in Multimedia.query.all()])

######### Inserciones #############
@app.route('/nuevo_usuario', methods=['GET', 'POST'])
def nuevo_usuario():
    # TODO: Controlar valores enviados a la app
    if request.method == 'POST':
        # extrayendo datos del request
        nombre = request.form['nombre']
        passw = request.form['passw']
        hpassw = hashlib.md5(passw).hexdigest()
        tipo = request.form['tipo']

        if nombre is None or len(passw) < 5:
            return "0" # no pueden ser vacios

        # comprobando si no existe
        if existe_nomb_usuario(nombre) is False:
            u = Usuario(nombre, hpassw, tipo)
            
            db_session.rollback()
            db_session.add(u)

            # TODO: manejar errores en la insercion
            db_session.commit()
            return "1" # TRUE
            #return redirect("ls_usuarios", code=302)
        else:
            return "0" # FALSE
            #return redirect("ls_usuarios", code=302)


@app.route('/nueva_queja', methods=['GET', 'POST'])
def nueva_queja():
    # TODO: Controlar valores enviados a la app
    if request.method == 'POST':
        # extrayendo datos del request
        id_usuario = request.form['id_usuario']
        titulo = request.form['titulo']
        descripcion = request.form['descripcion']
        categoria = request.form['categoria']
        
        # comprobacion
        if id_usuario == "":
            return "Error: id_usuario no puede ser null"
            #render_template('error.html', texto_error="no puede ser nulo el id de usuario")
        if titulo == "":
            return "Error: Titulo no puede ser vacio"
        if len(descripcion) <= 15:
            return "Error: Debe tener contenido mas de 15 cars"
        if categoria == "":
            categoria = 1
        
        q = Queja(id_usuario, titulo, descripcion, \
                    datetime.datetime.now(), 0, 0, categoria, 1)

        db_session.rollback()
        db_session.add(q)

        # TODO: manejar errores en la insercion
        db_session.commit()

        # --Llenado de la tabla registro en la BD --
        # TODO: comprobar si existe ya la queja (edicion)
        #       ademas comprobar que el usuario sea el autor.

        # accion = 0 : Queja, tipo = 0 : Insercion
        r = Registro(id_usuario, 0 , descripcion, 0, \
                 datetime.datetime.now())

        db_session.add(r) 
        db_session.commit()
        # ----

        return redirect("ls_quejas", code=302)

@app.route('/nuevo_comentario', methods=['GET', 'POST'])
def hacer_comentario():
    if request.method == 'POST':
        id_usuario = request.form['id_usuario']
        id_queja = request.form['id_queja']
        contenido = request.form['contenido']

        # comprobacion
        if id_usuario == "":
            #return "Error: id_usuario no puede ser null"
            return "0"
            #render_template('error.html', texto_error="no puede ser nulo el id de usuario")
        if id_queja == "":
            #return "Error: id_queja no puede ser vacio"
            return "0"
        if len(contenido) == 0 or contenido=="\t\t\t":
            #return "El contenido del comentario no puede ser vacio"
            return "0"
        
        if verif_queja(int(id_usuario), int(id_queja)) is False:
            return "0"
        
        # insercion en la BD
        db_session.rollback()
        c = Comentario(id_usuario, id_queja, contenido, \
                       datetime.datetime.now())
        db_session.add(c)
        # TODO: manejar errores en la insercion
        db_session.commit()
        
        return "1"
        #return redirect("ls_comentarios", code=302)

# TODO: cifrar password

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template("login.html", msj1="")
    elif request.method == 'POST':
        nomb = request.form['nombre']
        passw = request.form['passw']

        # comprueba si el usuario tiene sesion abierta
        if g.user is not None and g.user.is_authenticated:
            return redirect(url_for('pag_principal'))
        # comprueba que el usuario no este ya logueado
        if 'username' in session:
            if session['username'] == nomb:
                return render_template("index.html", usuario_logueado=session['username'])
        else:
            print "usuario", nomb, "no tiene abierta sesion"
            # return 'Logged in as %s' % escape(session['username'])
            if verif_Login(nomb, passw):
                # llenar la tabla de registro de ingresos
                # ingreso
                id_usuario = get_id_usuario(nomb)
                # TODO: registrar IP usando server web
                ip = '127.0.0.1'

                i = Ingreso(id_usuario, datetime.datetime.now(), ip)
                    
                db_session.rollback()
                db_session.add(i)
                db_session.commit()
                
                # agrega en el registro de usuarios logueados ahora
                session['username'] = nomb
                    
                flash("Usuario verificado satisfactoriamente")
                    
                #print url_for('login')
                return render_template("index.html", usuario_logueado=nomb)
            else:
                return render_template("login.html", login_e="Usuario o password no existen")
                

@app.route('/logout')
def logout():
    session.pop('username', None)
    # TODO: redireccionar a la pagina que estaba antes
    return redirect(url_for('pag_principal'))

@app.route('/registrarse')
def registrarse():
    return render_template("registrarse.html")

# @app.route('like', methods=['GET', 'POST'])
# def sumar_like():
#     if request.method == 'POST': # agregar dislike
#         return 

@app.route('/todas_quejas')
def todas_quejas():
    lq = Queja.query.all()
    lista_quejas = []
    for q in lq:
        print q
        lista_quejas.append([q.id_queja, \
                             get_nombre_usuario(q.id_usuario),\
                             q.titulo,\
                             q.descripcion, q.fecha_hora, q.likes,\
                             q.dislikes, q.categoria])
        #lista_quejas.append(q)
    print lista_quejas
    return render_template("prueba_quejas.html", \
                           lista_quejas=lista_quejas)

@app.route('/hola')
def hola():
    return "Hola"

###### Funciones de utilidad #######
def verif_Login(nombre=None, passw=None):
    hashpass = hashlib.md5(passw).hexdigest()
    print "solicitando pass: ", hashpass
    lu = Usuario.query.all()
    for u in lu:
        if u.nombre == nombre and u.passw == hashpass:
            print "confirmado usuario:", nombre
            return True
    return False

def existe_nomb_usuario(nombre=None):
    lu = Usuario.query.all()
    for u in lu:
        if u.nombre == nombre:
            return True
    return False

def verif_queja(id_usuario=0, id_queja=0):
    lq = Queja.query.all()
    for q in lq:
        if q.id_usuario == id_usuario and \
           q.id_queja == id_queja:
                return True
    return False

def existe_id_usuario(user_id=0):
    lu = Usuario.query.all()
    for u in lu:
        if u.id_usuario == user_id:
            return True
    return False

def existe_id_queja(id_queja=0):
    lq = Queja.query.all()
    for q in lq:
        print q.id_queja
        if q.id_queja == id_queja:
            return True
    return False

def get_id_usuario(nombre=None):
    lu = Usuario.query.all()
    for u in lu:
        if u.nombre == nombre:
            return u.id_usuario
    return -1

def get_nombre_usuario(id_usuario):
    lu = Usuario.query.all()
    for u in lu:
        if u.id_usuario == id_usuario:
            return u.nombre
    return "Anonimo"

def get_id_queja_desde_titulo(titulo=None):
    lq = Queja.query.all()
    for q in lq:
        if q.titulo == titulo:
            return q.id_queja
    return -1

def get_titulo_desde_id_queja(id_queja=0):
    lq = Queja.query.all()
    for q in lq:
        if q.id_queja == id_queja:
            return q.titulo
    return -1

### Programa principal
if __name__ == '__main__':

    init_db()
    login_manager.init_app(app)

    app.debug = True
    app.run()
