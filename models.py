# -*- coding: utf8 -*-
from sqlalchemy import Column, Integer, String, ForeignKey, Text, Binary, DateTime
from database import Base

def dump_datetime(value):
    """Deserialize datetime object into string form for JSON processing."""
    if value is None:
        return None
    return [value.strftime("%Y-%m-%d"), value.strftime("%H:%M:%S")]

class Usuario(Base):
    __tablename__ = 'usuario'
    id_usuario = Column(Integer, primary_key=True)
    nombre = Column(String(20), unique=True)
    passw = Column(String(60))
    tipo = Column(Integer)

    def __init__(self, nombre=None, passw=None, tipo=0):
        self.nombre = nombre
        self.passw = passw
        self.tipo = tipo

    def __repr__(self):
        return '<Usuario %r>' % (self.nombre)
    
    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    # agregado para serializar JSON
    # fuente: 
    # http://stackoverflow.com/questions/7102754/jsonify-a-sqlalchemy-result-set-in-flask
    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'id_usuario' : self.id_usuario,
            'nombre' : self.nombre,
            'passw' : self.passw,
            'tipo' : self.tipo
        }
    #     return {
    #        'id_usuario'         : self.id_usuario#,
    #         #'modified_at': dump_datetime(self.modified_at),
    #        # This is an example how to deal with Many2Many relations
    #        #'many2many'  : self.serialize_many2many
    #     }
    # @property
    # def serialize_many2many(self):
    #    """
    #    Return object's relations in easily serializeable format.
    #    NB! Calls many2many's serialize property.
    #    """
    #    return [ item.serialize for item in self.many2many]

class Queja(Base):
    __tablename__ = 'queja'
    id_queja = Column(Integer, primary_key=True)
    id_usuario = Column(Integer, ForeignKey('usuario.id_usuario'))
    titulo = Column(String(160), unique=True)
    descripcion = Column(Text)
    fecha_hora = Column(DateTime)
    likes = Column(Integer)
    dislikes = Column(Integer)
    categoria = Column(String(30))
    visible = Column(Integer)
    
    def __init__(self, id_usuario=0, titulo=None, \
                 descripcion=None, fecha_hora=None, likes=0,\
                 dislikes=0, categoria=None, visible=0):
        self.id_usuario=id_usuario
        self.titulo= titulo
        self.descripcion = descripcion
        self.fecha_hora = fecha_hora
        self.likes = likes
        self.dislikes = dislikes
        self.categoria = categoria
        self.visible = visible
    
    def __repr__(self):
        return '<Queja %r>' % (self.id_queja)
    
    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'id_queja'         : self.id_queja,
            'id_usuario': self.id_usuario,
            'titulo': self.titulo,
            'descripcion': self.descripcion,
            'fecha_hora' : dump_datetime(self.fecha_hora),
            'likes' : self.likes,
            'dislikes' : self.dislikes,
            'categoria' : self.categoria,
            'visible' : self.visible
        }
        
class Comentario(Base):
    __tablename__ = 'comentario'
    id_comentario = Column(Integer, primary_key=True)
    id_usuario = Column(Integer, ForeignKey('usuario.id_usuario'))
    id_queja = Column(Integer, ForeignKey('queja.id_queja'))
    contenido = Column(Text)
    fecha_hora = Column(DateTime)
    
    def __init__(self, id_usuario=99999, id_queja=99999,\
                 contenido=None, fecha_hora=None):
        self.id_usuario = id_usuario
        self.id_queja
        self.contenido = contenido
        self.fecha_hora = fecha_hora

    def __repr__(self):
        return '<Comentario %r>' % (self.id_comentario)
        
    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'id_comentario' : self.id_comentario,
            'id_usuario' : self.id_usuario,
            'id_queja'         : self.id_queja,
            'contenido' : self.contenido,
            'fecha_hora' : dump_datetime(self.fecha_hora)
        }

class Registro(Base):
    __tablename__ = 'registro'
    id_reg = Column(Integer, primary_key=True)
    id_usuario = Column(Integer, ForeignKey('usuario.id_usuario'))
    accion = Column(Integer)
    contenido = Column(Text)
    tipo = Column(Integer)
    fecha_hora = Column(DateTime)

    def __init__(self, id_usuario=989999, accion=0,contenido=None,\
                 tipo=0, fecha_hora=None):
        self.id_usuario = id_usuario
        self.accion = accion
        self.contenido = contenido
        self.tipo = tipo
        self.fecha_hora = fecha_hora

    def __repr__(self):
        return '<Registro %r>' % (self.id_reg)

    @property
    def serialize(self):
        return {
            'id_ref' : self.id_reg,
            'id_usuario' : self.id_usuario,
            'accion'         : self.accion,
            'contenido' : self.contenido,
            'tipo': self.tipo,
            'fecha_hora' : dump_datetime(self.fecha_hora)
        }
    
class Ingreso(Base):
    __tablename__ = 'ingreso'
    id_ingreso = Column(Integer, primary_key=True)
    id_usuario = Column(Integer, ForeignKey('usuario.id_usuario'))
    fecha_hora = Column(DateTime)
    ip_ingreso = Column(String(92))

    def __init__(self, id_usuario=999999, fecha_hora=None,\
                 ip_ingreso=None):
        self.id_usuario = id_usuario
        self.fecha_hora = fecha_hora
        self.ip_ingreso = ip_ingreso

    def __repr__(self):
        return '<Ingreso %r>' % (self.id_ingreso)
    
    @property
    def serialize(self):
        return {
            'id_ingreso' : self.id_ingreso,
            'id_usuario' : self.id_usuario,
            'fecha_hora' : dump_datetime(self.fecha_hora),
            'ip_ingreso' : self.ip_ingreso
        }

class Multimedia(Base):
    __tablename__ = 'multimedia'
    id_multi = Column(Integer, primary_key=True)
    id_queja = Column(Integer, ForeignKey('queja.id_queja'))
    tipo = Column(Integer)
    nombre = Column(String(30))

    def __init__(self, id_queja=9999991, tipo=1, nombre=None):
        self.id_queja = id_queja
        self.tipo = tipo
        self.nombre = nombre
    
    def __repr__(self):
        return '<Multimedia %r>' % (self.id_multi)

    @property
    def serialize(self):
        return {
            'id_multi' : self.id_multi,
            'id_queja' : self.id_queja,
            'ip_tipo' : self.tipo,
            'nombre' : self.nombre
        }    
    

    

