## Portal de Reclamos ##

Una aplicación para que personas realizen reclamos libremente. La idea es que una persona (no necesariamente un usuario de la app) pueda postear reclamos describiendo su reclamo. Estos reclamos son visibles públicamente. Los reclamos son autoregulados, a través de un sistema de validación. Los usuarios votan si un reclamo les parece válido y también si les parece inválido.

Si un reclamo tiene muchos votos en contra, ya no es visible para el público o también si el reclamo no tiene ningún voto y pasa cierto tiempo.

Los usuarios pueden tambíen hacer comentarios y subir fotos para describir mejor sus reclamos.

LICENCIA: *AGPL*
EJECUTAR: Ver [Docu/setup_inicial.txt](Docu/setup_inicial.txt)
